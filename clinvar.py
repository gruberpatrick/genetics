
import urllib.request
import json
from time import sleep
import os
import sys
import re
from pathlib import Path

class ClinVar:

  command = ""

  # ------------------------------------------------------------------------------
  def getUniqueValuesFromCSV(self, file, position):
    res = {}
    with open(file) as infile:
      for line in infile:
        values = line.split(",")
        if len(values) <= position: continue
        res[values[position]] = 1
    return [*res]

  # ------------------------------------------------------------------------------
  def getGeneClinVarIDs(self, gene):
    try:
      url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=clinvar&term=" + gene + "[gene]&retmax=500&retmode=json"
      r = urllib.request.urlopen(url)
      return json.loads(r.read().decode())
    except:
      return -1

  # ------------------------------------------------------------------------------
  def getGeneClinVarDetails(self, ID):
    try:
      url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=clinvar&id=" + ID + "&retmode=json"
      r = urllib.request.urlopen(url)
      return re.sub(r"[\n\t\s]*", "", r.read().decode())
    except:
      return -1

  # ------------------------------------------------------------------------------
  def saveFile(self, file, data):
    f = open(file, "w+")
    lines = f.write(data)
    f.close()

  # ------------------------------------------------------------------------------
  def createFolder(self, ):
    try:
      os.mkdir("./clinvar_data")
    except:
      return -1

  # ------------------------------------------------------------------------------
  def getData(self, file, column):
    genes = self.getUniqueValuesFromCSV(file, column)
    for i in genes:
      WL.l("CLINVAR", "GETTING " + i)
      ids = -1
      while ids == -1:
        ids = self.getGeneClinVarIDs(i)
        for j in ids["esearchresult"]["idlist"]:
          if Path("./clinvar_data/" + i + "." + j + ".txt").is_file():
            WL.l("CLINVAR", "ALREADY EXISTS " + j + ". Ignoring.", "warning")
            continue
          WL.l("CLINVAR", "Creating '" + i + "." + j + ".txt'.")
          detail = -1
          while detail == -1:
            detail = self.getGeneClinVarDetails(j)
            if detail != -1:
              self.saveFile("./clinvar_data/" + i + "." + j + ".txt", detail)
              WL.l("CLINVAR", "Done.", "success")
            sleep(1)
      sleep(1)

  # ------------------------------------------------------------------------------
  def getGeneFiles(self, gene):
    res = []
    for filename in os.listdir("./clinvar_data/"):
      try:
        if filename.index(gene) != 0: continue
        res.append(filename)
      except:
        continue
    return res

  # ------------------------------------------------------------------------------
  def readGeneFiles(self, file):
    f = open(file, "r+")
    content = f.read()
    f.close()
    return json.loads(content)

  # ------------------------------------------------------------------------------
  def analyzeGene(self, gene):
    files = self.getGeneFiles(gene)
    for i in files:
      data = self.readGeneFiles("./clinvar_data/" + i)
      for j in data["result"]["uids"]:
        if data["result"][j]["clinical_significance"]["description"] == "Pathogenic":
          print("###########################################################")
          print( i + " - " + data["result"][j]["clinical_significance"]["description"] )
          for x in data["result"][j]["variation_set"]:
            for y in x["variation_loc"]:
              print( y["assembly_name"] + ": " +  y["start"] + "-" + y["stop"] + " => " + y["ref"] + " vs " + y["alt"])

  # ------------------------------------------------------------------------------
  def controller(self, args):
    self.command = args[1]
    self.createFolder()
    if self.command == "-h":
      WL.l("CLINVAR", "Get ClinVar data with given CSV file.\npython3.5 clinvar.py COMMAND [FILE] [COLUMN]\n\n-v : get version\n\n-g : get data \n[FILE] The file containing all the genes.\n[COLUMN] The column containing all the genes. Do not have to be unique.\n\n", "error")
    elif self.command == "-g":
      if len(args) != 4:
        WL.l("CLINVAR", "Need to provide FILE and COLUMN for this operation. Use '-h' for help.", error)
        return -1
      self.getData(args[2], int(args[3]))
    elif self.command == "-a":
      if len(args) != 3:
        WL.l("CLINVAR", "Need to provide GENE for this operation. Use '-h' for help.", "error")
        return -1
      self.analyzeGene(args[2])
    return 1

# ------------------------------------------------------------------------------
# make tool available for automation with other scripts
if __name__ == '__main__':
  e = ClinVar()
  e.controller(sys.argv)
