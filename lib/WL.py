
wl_verbose = False

wl_yellow =   "\x1b[6;30;93m "
wl_red =      "\x1b[6;30;31m "
wl_green =    "\x1b[6;30;32m "
wl_rs =       " \x1b[0m"

class WL:

  def disable():
    global wl_verbose
    wl_verbose = True

  def enable():
    global wl_verbose
    wl_verbose = False

  def l(fr, text, type=""):
    global wl_verbose, wl_yellow, wl_red, wl_green, wl_rs
    if(wl_verbose): return -1
    value = ""
    if(type == "warning"):
      value = wl_yellow + "[" + fr + "][WARNING]"
    elif(type == "error"):
      value = wl_red + "[" + fr + "][ERROR]"
    elif(type == "success"):
      value = wl_green + "[" + fr + "][SUCCESS]"
    else:
      value = " [" + fr + "]"
    print(value + wl_rs + text)
