
import os
import sys
import urllib.request
import json
from pathlib import Path
from lib.WL import WL

class Ensembl:

  command = ""

  # ------------------------------------------------------------------------------
  def getData(self, chromosome, start, end, features):
    f = ""
    p = "./ensembl_data/"+chromosome+":"+start+"-"+end+":"+features+".csv"
    if Path(p).is_file():
      WL.l("ENSEMBL", "ALREADY EXISTS '"+chromosome+":"+start+"-"+end+":"+features+".csv'. Ignoring.", "warning")
      return (p, -1)
    WL.l("ENSEMBL", "Creating '" + chromosome+":"+start+"-"+end+":"+features+".csv'.")
    file = open(p, "w+")
    features = features.split(",")
    for i in features:
      f += "feature=" + i + ";"
    url = "http://rest.ensembl.org/overlap/region/human/" + str(chromosome) + ":" + str(start) + "-" + str(end) + "?" + f + "content-type=application/json"
    r = urllib.request.urlopen(url)
    genes = json.loads(r.read().decode())
    for g in genes:
      if("gene_id" not in g): continue
      file.write(str(g["gene_id"])+","+str(g["external_name"])+","+str(g["start"])+","+str(g["end"])+","+str(g["assembly_name"])+","+str(g["feature_type"])+"\n")
    file.close()
    WL.l("ENSEMBL", "Done.", "success")
    return (p, 1)

  # ------------------------------------------------------------------------------
  def getLocationFile(self, file):
    ret = []
    with open(file) as infile:
      for line in infile:
        res = {}
        line = line.strip()
        res["chr"] = line[0:line.index(":")]
        res["start"] = line[line.index(":")+1:line.index("-")]
        res["end"] = line[line.index("-")+1:line.index(";")]
        res["features"] = line[line.index(";")+1:]
        ret.append(res)
    return ret

  # ------------------------------------------------------------------------------
  def createFolder(self):
    try:
      os.mkdir("./ensembl_data")
    except:
      return -1

  # ------------------------------------------------------------------------------
  def controller(self, args):
    self.command = args[1]
    self.createFolder()
    if self.command == "-h":
      WL.l("ENSEMBL","Get Ensembl data from given region.\npython3.5 ensembl.py COMMAND [OPTIONS]\n\n")
    elif self.command == "-g":
      if(len(args) != 3):
        WL.l("ENSEMBL","Command -g requires the FILE parameter. Use -h to get more information.", "error")
        return -1
      locations = self.getLocationFile(args[2])
      res = []
      for l in locations:
        res.append( self.getData(l["chr"], l["start"], l["end"], l["features"]) )
      return res
    elif self.command == "-gl":
      if(len(args) != 6):
        WL.l("ENSEMBL", "Command -gl requires CHROMOSOME, START, END, [FEATURES] parameters. Use -h to get more information.", "error")
        return -1
      return self.getData(args[2], args[3], args[4], args[5])
    return 1

# ------------------------------------------------------------------------------
# make tool available for automation with other scripts
if __name__ == '__main__':
  e = Ensembl()
  e.controller(sys.argv)
